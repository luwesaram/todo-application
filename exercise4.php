<?php

class API {

  function outputName($name){
    echo "Full Name: $name \n";
  }

  function outputHobbies(array $arrHobbies){
    echo "Hobbies: \n";
    foreach ($arrHobbies as $hobby){
      echo "\t $hobby \n";
    }
  }

  function outputInformation(object $information){
    echo "Age: $information->age \n";
    echo "Email: $information->email \n";
    echo "Birthday: $information->birthday \n";
  }
}

$name = "Jose Luis N. Arambulo";
$hobbies = ['Gaming', 'Watching Anime', 'Reading Manga'];
$info = (object) [
  'age' => 22,
  'email' => 'luwesaram@gmail.com',
  'birthday' => 'August 25, 2001'
];

$obj = new API();

$obj->outputName($name);
$obj->outputHobbies($hobbies);
$obj->outputInformation($info);

?> 