<?php
// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'employee';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Insert data into the employee table
echo "Insert data into the employee table:" . "<br> <br>";
$sql = "INSERT INTO EMPLOYEE VALUES (NULL, 'Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully" . "<br>";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// retrieve the first name, last name, and birthday of all employees in the table
echo "Retrieve the first name, last name, and birthday of all employees in the table:" . "<br> <br>";
$sql =  "SELECT first_name, last_name, birthday FROM EMPLOYEE";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
    }
} else {
    echo "0 results";
}

// retrieve the number of employees whose last name starts with the letter 'D'

echo "Retrieve the number of employees whose last name starts with the letter 'D':" . "<br> <br>";
$sql =  "SELECT COUNT(id) as number_of_employees FROM employee WHERE last_name LIKE 'D%'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "Number of employees with last name starting with letter D:" . $row['number_of_employees'] . "<br>";
    }
} else {
    echo "0 results";
}


//  retrieve the first name, last name, and address of the employee with the highest ID number

echo "Retrieve the first name, last name, and adress of the employee with the highest ID number" . "<br> <br>";
$sql =  "SELECT first_name, last_name, address FROM employee WHERE id = (SELECT MAX(id) FROM employee)";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "0 results";
}


// Update data in the employee table
echo "Update data in the employee table" . "<br> <br>";
$sql =  "UPDATE employee SET address = '123 Main Street' WHERE first_name = 'John'";
if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully" . "<br>";
} else {
    echo "Error updating record: " . $conn->error;
}


// Delete data from the employee table
echo "Delete data in the employee table" . "<br> <br>";
$sql = "DELETE FROM employee WHERE last_name LIKE 'D%'";
if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}
$conn->close();

?>