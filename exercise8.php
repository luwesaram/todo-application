<?php
  require_once('MysqliDb.php');
  
  $host = 'localhost';
  $username = 'root';
  $password = '';
  $database = 'employee';

  $rows = [];
  $selected_id = NULL;
  $selected_fname = NULL;
  $selected_lname = NULL;
  $selected_mname = NULL;
  $selected_birthday = NULL;
  $selected_address = NULL;

  $conn = new mysqli($host, $username, $password, $database);
  $db = new MysqliDb ($conn);

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    if (isset($_POST['view'])) {    

      $rows = $db->get('employee');
      if ($db->count > 0){
        echo "Displayed data. Showing $db->count result(s)." . "<br>";
      }
      else {
        echo "Error.";
      }
    }

    if (isset($_POST['create'])){
      $data = Array (
        "first_name" => $_POST['create_first_name'],
        "last_name" => $_POST['create_last_name'],
        "middle_name" => $_POST['create_middle_name'],
        "birthday" => $_POST['create_birthday'],
        "address" => $_POST['create_address']
      );

      $result = $db->insert('employee', $data);

      if ($result){
        echo "Created data." . "<br>";
      }
      else {
        echo "Error." . $db->getLastError() . "<br>";
      }
    }

    if (isset($_POST['delete'])){
      $id = $_POST['ud_id'];
      if ($id){

        $db->where('id', $id);
        $result = $db->delete('employee');
  
        if ($result){
          echo "Deleted data." . "<br>";
        }
        else {
          echo "Error." . $db->getLastError() . "<br>";
        }
      }
      else {
        echo "Select an ID to delete"; 
      }
    }

    if (isset($_POST['select'])){
      $selected_id = $_POST['selected_id'];
      $selected_fname = $_POST['selected_fname'];
      $selected_lname = $_POST['selected_lname'];
      $selected_mname = $_POST['selected_mname'];
      $selected_birthday = $_POST['selected_birthday'];
      $selected_address = $_POST['selected_address'];
    }

    if (isset($_POST['update'])){
      $id = $_POST['ud_id'];
      if ($id){
        $data = Array (
          "first_name" => $_POST['ud_first_name'],
          "last_name" => $_POST['ud_last_name'],
          "middle_name" => $_POST['ud_middle_name'],
          "birthday" => $_POST['ud_birthday'],
          "address" => $_POST['ud_address']
        );
        $db->where('id', $id);
        $result = $db->update('employee', $data);

        if ($result){
          echo "Updated data." . "<br>";
        }
        else {
          echo "Error." . $db->getLastError() . "<br>";
        }
      }
      else {
        echo "Select an ID to update"; 
      }
    }
  } 


?>

<div style="margin: auto; display: flex;">
<div style="margin-right: 200;  ">
  <form method="POST">
      <h2>Create Data</h2>

      <label for='create_first_name'> Enter your First Name: </label>
      <input type="text" id="create_first_name" name="create_first_name" required>
      <br><br>

      <label for='create_last_name'> Enter your Last Name: </label>
      <input type="text" id="create_last_name" name="create_last_name" required>
      <br><br>

      <label for='create_middle_name'> Enter your Middle Name: </label>
      <input type="text" id="create_middle_name" name="create_middle_name" required>
      <br><br>

      <label for='create_birthday'> Enter your birthday: </label>
      <input type="text" id="create_birthday" name="create_birthday" required>
      <br><br>

      <label for='create_address'> Enter your address: </label>
      <input type="text" id="create_address" name="create_address" required>
      <br><br>

      <button type="submit" name="create"> Submit </button>
  </form>
</div>

<div style="float: left; width: 50%;">
  <form method="POST">
    <h2>Update or Delete</h2>
    <p> 
      <?php
          echo "Selected ID: "; 
          if ($selected_id === NULL) {
          echo "None";
          }
          else {
            echo $selected_id;
          }
      ?>  
    </p>
    <input type='hidden' name='ud_id' value='<?php echo $selected_id; ?>' required>

    <label for='ud_first_name'> Enter your First Name: </label>
    <input type="text" id="ud_first_name" name="ud_first_name" value='<?php echo $selected_fname ?>'>
    <br><br>

    <label for='ud_last_name'> Enter your Last Name: </label>
    <input type="text" id="ud_last_name" name="ud_last_name" value='<?php echo $selected_lname ?>'>
    <br><br>

    <label for='ud_middle_name'> Enter your Middle Name: </label>
    <input type="text" id="ud_middle_name" name="ud_middle_name" value='<?php echo $selected_mname ?>'>
    <br><br>

    <label for='ud_birthday'> Enter your birthday: </label>
    <input type="text" id="ud_birthday" name="ud_birthday" value='<?php echo $selected_birthday?>'>
    <br><br>

    <label for='address'> Enter your address: </label>
    <input type="text" id="ud_address" name="ud_address" value='<?php echo $selected_address?>'>
    <br><br>

    <button type="submit" name="update"> Update </button>
    <button type="submit" name="delete"> Delete </button>
  </form>
</div>
</div>

<h2>Data</h2>
<form method="post">
  <input type="submit" name="view" value="View All">
</form>
<?php foreach ($rows as $row): ?>
  <div style="border: 1px solid black; margin-bottom: 5px; display: flex; justify-content: space-between; width: 65%;">
    <div>
      <?php echo "ID: " .$row["id"] . " - First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Middle Name: " . $row["middle_name"].  " - Birthday: " . $row["birthday"] . " - Address: " . $row["address"]?>
    </div>
    <div style="margin-right: 3px;">
      <form method='POST'>
        <input type='hidden' name='selected_id' value='<?php echo $row['id']; ?>'>
        <input type='hidden' name='selected_fname' value='<?php echo $row['first_name']; ?>'>
        <input type='hidden' name='selected_lname' value='<?php echo $row['last_name']; ?>'>
        <input type='hidden' name='selected_mname' value='<?php echo $row['middle_name']; ?>'>
        <input type='hidden' name='selected_birthday' value='<?php echo $row['birthday']; ?>'>
        <input type='hidden' name='selected_address' value='<?php echo $row['address']; ?>'>
        <input type='submit' name='select' value='Select'>
      </form>
    </div>
  </div> 
<?php endforeach; ?>