<?php
  require_once('MysqliDb.php');
  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
  header("Access-Control-Allow-Headers: Content-Type");

  class API {
    private $db;
    public function __construct() 
    {
        $this->db = new MysqliDb('localhost', 'root', '', 'todolist');
    }
    
    public function httpGet($payload)
    {
      $payload = $_GET; // for some reason without this line codeception gives error and assumes that payload as null
                              // but on Postman/Insomnia adding this will give an error, and without this it will succeed.
      if (!is_array($payload)) {
        echo "Payload is not an array.";
      }
      else {
        $this->db->where('id', $payload['id'], 'IN');
        $data = $this->db->get('tbl_to_do_list');
        if ($data){
          echo json_encode(array(
            'method' => 'GET',
            'status' => 'success',
            'data'=> $data,
          ));
        }
        else {
          echo json_encode(array(
            'method' => 'GET',
            'status' => 'failed',
            'message' => 'Failed to Fetch Request',
          ));
        }
      }
    }

    public function httpPost($payload) 
    {
      if(is_array($payload) && !empty($payload)){
        $data = $this->db->insert('tbl_to_do_list', $payload);
        if($data) {
          echo json_encode(array(
            'method' => 'POST',
            'status' => 'success',
            'data'=> $data,
          ));
        }

        else {
          echo json_encode(array(
            'method' => 'POST',
            'status' => 'failed',
            'message' => 'Failed to Insert Data',
          ));
        }
      }

      else {
        echo "Payload is not an array or it is an empty array.";
      }
    }

    public function httpPut($id, $payload){
      if (($id) && ($payload) && ($payload['id'] == $id)) {
        $this->db->where('id', $id);
        $data = $this->db->update('tbl_to_do_list', $payload);

        if ($data) {
          echo json_encode(array(
            'method' => 'PUT',
            'status' => 'success',
            'data'=> $data,
          ));
        }
        else {
          echo json_encode(array(
            'method' => 'PUT',
            'status' => 'failed',
            'message' => 'Failed to Update Data',
          ));
        }
      }
      else {
        echo "ID or payload is empty, or payload ID doesn't match ID\n"; 
      }
    }

    public function httpDelete($id, $payload){
      if(($id) && ($payload)){ // check if payload and id is not null or empty
        if(is_array($payload['id'])) { // check if payload is an array, prepare to delete multiple id
          if(in_array($id, $payload['id'])){ // check if id is inside payload, proceed to delete
            $this->db->where('id', $payload['id'], 'IN');
            $result = $this->db->delete('tbl_to_do_list');
  
            if ($result) {
              echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'success',
                'data'=> $payload,
                'message' => 'deleted data',
              ));
            }
            else {
              echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Failed to Delete Data',
              ));
            }
          }
          else {
            echo "Payload doesnt match ID.";
          }
        }

        elseif ($payload['id'] == $id){ // if payload is not an array, payload must be equal to id
          $this->db->where('id', $id);
          $result = $this->db->delete('tbl_to_do_list');

          if($result){
            echo json_encode(array(
              'method' => 'DELETE',
              'status' => 'success',
              'data'=> $id,
              'message' => 'deleted one data',
            ));
          }

          else {
            echo json_encode(array(
              'method' => 'DELETE',
              'status' => 'failed',
              'message' => 'Failed to Delete Data',
            ));
          }
        }

        else { // statement for payload not matching ID
          echo "Payload doesnt match ID.";
        }
      }

      else {
        echo "Payload or ID is empty.";
      }
    }
  }

  $request_method = $_SERVER['REQUEST_METHOD'];
  
  if ($request_method === 'GET') {
    $received_data = $_GET;
  } 

  if ($request_method === 'POST') {
    $received_data = $_POST;
  }
  
  else {
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
      $request_uri = $_SERVER['REQUEST_URI'];
      
      $ids = null;
      
      $exploded_request_uri = array_values(explode("/",$request_uri));
      
      $last_index = count($exploded_request_uri) - 1;
      
      $ids = $exploded_request_uri[$last_index];

      $ids = intval($ids); //convert string to int
    }
  }
  
  $received_data = json_decode(file_get_contents('php://input'), true);
 
  $api = new API;

   switch ($request_method) {
       case 'GET':
          $api->httpGet($received_data);
          break;
       case 'POST':
          $api->httpPost($received_data);
          break;
       case 'PUT':
          $api->httpPut($ids, $received_data);
          break;
       case 'DELETE':
          $api->httpDelete($ids, $received_data);
          break;
   }

?>