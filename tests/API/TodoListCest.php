<?php


namespace Tests\API;

use Tests\Support\APITester;

class TodoListCest
{
    public function iShouldCreateTask(APITester $I) // Scenario 2
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/todo-application/API.php/', [
            'id' => 15,
            'task_title' => 'Sample Task',
            'task_name' => 'Sample Name',
            'time' => '2024-04-01 03:07:10.000000',
            'status' => 'Inprogress'
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldGetTask(APITester $I) // Scenario 3
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/todo-application/API.php/', [
            'id' => ['1','15']
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldUpdateTaskStatus(APITester $I) // Scenario 4 & 5
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/todo-application/API.php/15', [
            'id' => '15',
            'status' => 'Done' // Can be 'Inprogress' as well
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldEditTaskDetails(APITester $I) // Scenario 6
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/todo-application/API.php/15', [
            'id' => '15',
            'task_title' => 'Updated Task Title',
            'task_name' => 'Updated Task Name',
            'time' => '2024-04-02 03:07:10.000000'
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldDeleteTasks(APITester $I) // Scenario 7
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/todo-application/API.php/15', [
            'id' => '15'
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldDeleteTaskArray(APITester $I) // Scenario 7 but array , to test if delete function can handle array
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/todo-application/API.php/15', [
            'id' => ['15']
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

}
